FROM nats-streaming:0.9.2-linux as nats
MAINTAINER Arve Knudsen <arve.knudsen@gmail.com>

FROM python:3.6-alpine

WORKDIR /app
ENTRYPOINT ["./run-nats-streaming"]

COPY run-nats-streaming .
COPY --from=nats /nats-streaming-server /
