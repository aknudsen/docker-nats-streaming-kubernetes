# docker-nats-streaming-kubernetes
Dockerfile for NATS Streaming under Kubernetes. The resulting Docker image is meant for running
as part of a Kubernetes StatefulSet, in combination with
[NATS Operator](https://github.com/nats-io/nats-operator). The latter is necessary because
our NATS Streaming cluster must work on top of a NATS cluster, for example in order for cluster
peers to find each other.

## Deployment to Kubernetes
The Docker image should be deployed to a Kubernetes cluster as part of a StatefulSet.
We've included an example manifest for you to base yourself on in example/nats-streaming.yaml.
